package com.ad.bekuppertemuan2.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ad.bekuppertemuan2.Fragment.Fragment2;
import com.ad.bekuppertemuan2.Fragment.Fragment3;
import com.ad.bekuppertemuan2.Fragment.PlaceholderFragment;

/**
 * Created by Amalia on 19/10/2016.
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter {
    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        Fragment fragment= null;
        switch (position){
            case 0: fragment=new PlaceholderFragment();
                break;
            case 1: fragment= new Fragment2();
                break;
            case 2: fragment= new Fragment3();
                break;
            default:
                new PlaceholderFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Home";
            case 1:
                return "List";
            case 2:
                return "Login";
        }
        return null;
    }
}
