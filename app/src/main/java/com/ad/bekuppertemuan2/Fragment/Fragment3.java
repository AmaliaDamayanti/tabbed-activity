package com.ad.bekuppertemuan2.Fragment;


import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ad.bekuppertemuan2.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment3 extends Fragment {


    public Fragment3() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview= inflater.inflate(R.layout.fragment_fragment3, container, false);

            TextView textView= (TextView) rootview.findViewById(R.id.textView4);
            textView.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        return rootview;
    }

}
